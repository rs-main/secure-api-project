<ul class="navbar-nav">
    <li class="nav-item">
{{--        <a href="{{route("home")}}" class="nav-link dropdown-toggle active">Home</a>--}}
        <a href="{{route("home")}}" class="nav-link dropdown-toggle">Home</a>

    <li class="nav-item">
        <a href="{{route("about")}}" class="nav-link">About</a>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link dropdown-toggle">Personal <i class="bx bx-chevron-down"></i></a>
        <ul class="dropdown-menu">

            <li class="nav-item">
                <a href="{{route("personal.loans")}}" class="nav-link">Loans</a>
            </li>

            <li class="nav-item">
                <a href="{{route("personal.prestige")}}" class="nav-link">Prestige Banking<w/a>
            </li>

            <li class="nav-item">
                <a href="{{route("personal.savings")}}" class="nav-link">Savings & Investments</a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link dropdown-toggle">Commercial <i class="bx bx-chevron-down"></i></a>
        <ul class="dropdown-menu">

            <li class="nav-item">
                <a href="{{route("commercial.transactional")}}" class="nav-link">Transactional Accounts</a>
            </li>

            <li class="nav-item">
                <a href="{{route("commercial.digital")}}" class="nav-link">Digital Banking</a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a href="{{route("contact-us")}}" class="nav-link">Contact</a>
    </li>

</ul>
