@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="index.html" class="logo">
                <img src="assets/img/logo-two.png" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="/">
                        <img src="assets/img/logo.png" alt="Logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        @include("partials.side_nav")

                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Transactional Accounts</h2>
                        <ul>
                            <li>
                                <a href="{{route("home")}}">Home</a>
                            </li>
                            <li>
                                <span>Transactional Accounts</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>Transactional Accounts</h2>
                            <p>
                                A current account that allows businesses the freedom to manage their money according to their transactional needs.
                                This account provides an unrestricted access to clients’ money anytime and anywhere via our branches or electronic channels.
                            </p>
                        </div>
                        <div class="details-business">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details2.jpg" alt="Details">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details3.jpg" alt="Details">
                                    </div>
                                </div>
                            </div>

                            <h3>SME Current Account</h3>
                            <p>
                                The SME Current account is tailored to assist businesses to manage their daily transactions and cash management with ease and security.

                                Make trade payments, access foreign exchange, receive foreign remittances, and access a wide range of business
                                loans/ credit to meet specific business needs. Available in GHS, USD, GBP and EUR.
                            </p>

                            <h3>Commercial Current Account Plus</h3>
                            <p>
                                Our Commercial Current Account Plus is designed purposely for businesses that constantly maintain
                                balances in excess of a capped amount on a monthly basis.
                                The account rewards businesses for maintaining a minimum balance
                                of $5,000 every month, to enjoy a waiver of the monthly account maintenance fee.
                            </p>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="widget-area">
                        <div class="services widget-item">
                            <h3>Services List</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        Cash Investment
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Personal Insurance
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Education Loan
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Financial Planning
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="download widget-item">
                            <h3>Download</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class='bx bxs-file-pdf'></i>
                                        Presentation pdf
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class='bx bx-notepad'></i>
                                        Wordfile.doc
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="contact widget-item">
                            <h3>Contact</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email">
                                </div>
                                <div class="form-group">
                                    <textarea id="your-message" rows="8" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <button type="submit" class="btn common-btn">
                                    Send Message
                                    <span></span>
                                </button>
                            </form>
                        </div>
                        <div class="consultation">
                            <img src="assets/img/services/service-details4.jpg" alt="Details">
                            <div class="inner">
                                <h3>Need Any Consultation</h3>
                                <a class="common-btn" href="contact.html">
                                    Send Message
                                    <span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
