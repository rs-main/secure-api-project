<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/about', [App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('/services', [App\Http\Controllers\HomeController::class, 'services'])->name('services');

Route::get('/personal', [App\Http\Controllers\HomeController::class, 'personal'])->name('personal');
Route::get('/personal-loans', [App\Http\Controllers\HomeController::class, 'personalLoans'])->name('personal.loans');
Route::get('/personal-prestige', [App\Http\Controllers\HomeController::class, 'personalPrestige'])->name('personal.prestige');
Route::get('/personal-savings', [App\Http\Controllers\HomeController::class, 'personalSavings'])->name('personal.savings');

Route::get('/commercial', [App\Http\Controllers\HomeController::class, 'commercial'])->name('commercial');
Route::get('/commercial-transactional', [App\Http\Controllers\HomeController::class, 'commercialTransactional'])->name('commercial.transactional');
Route::get('/commercial-digital', [App\Http\Controllers\HomeController::class, 'commercialDigital'])->name('commercial.digital');

Route::get('/contact-us', [App\Http\Controllers\HomeController::class, 'contactUs'])->name('contact-us');

Route::get("/token",[App\Http\Controllers\HomeController::class, 'getToken'])->name("token");
